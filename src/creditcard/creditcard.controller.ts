import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res } from '@nestjs/common';
import { Response } from 'express';
import { Creditcard} from './creditcard';
import { CreditcardService } from './creditcard.service';

@Controller('creditcard')
export class CreditcardController {

    constructor(private creditcardService: CreditcardService) { }

    @Get()
    async findAll(@Res() res: Response) {
        const response = await this.creditcardService.findAll()
        res.status(HttpStatus.OK).json({ payload: response })
    }

    @Get(":id")
    async findOne(@Param() id: number, @Res() res: Response) {
        const response = await this.creditcardService.findOne(id)
        res.status(HttpStatus.OK).json({ payload: response })
    }

    @Post()
    async create(@Body() createUserDto: Creditcard, @Res() res: Response) {
        const response = await this.creditcardService.create(createUserDto)
        res.status(HttpStatus.OK).json({ payload: response })
    }

    @Put(":id")
    async update(@Param() id: number, @Body() createUserDto: Creditcard, @Res() res: Response) {
        this.creditcardService.update(id, createUserDto)
        res.status(HttpStatus.OK).json({ message: "success" })
    }

    @Delete(":id")
    async delete(@Param() id: number, @Res() res: Response) {
        this.creditcardService.remove(id)
        res.status(HttpStatus.OK).json({ message: "success" })
    }
}
