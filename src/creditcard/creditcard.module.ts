import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CreditcardController } from './creditcard.controller';
import { Creditcard } from './creditcard';
import { CreditcardService } from './creditcard.service';

@Module({
  imports: [TypeOrmModule.forFeature([Creditcard])],
  controllers: [CreditcardController],
  providers: [CreditcardService],
})
export class CreditcardModule { }