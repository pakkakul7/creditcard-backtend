import { Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Creditcard } from './creditcard';

@Injectable()
export class CreditcardService {
    constructor(
        @InjectRepository(Creditcard)
        private creditcardRepository: Repository<Creditcard>,
    ) { }

    findAll(): Promise<Creditcard[]> {
        return this.creditcardRepository.find();
    }

    findOne(id: number): Promise<Creditcard> {
        return this.creditcardRepository.findOne(id);
    }

    create(user: Creditcard): Promise<Creditcard> {
        return this.creditcardRepository.save(user);
    }

    async update(id: number, user: Creditcard) {
        await this.creditcardRepository.update(id, user)
    }

    async remove(id: number): Promise<void> {
        await this.creditcardRepository.delete(id);
    }


}
