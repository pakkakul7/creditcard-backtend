import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Creditcard {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  card_number: string;

  @Column()
  card_name: string;

  @Column()
  expmm: string;

  @Column()
  expyy: string;

  @Column()
  cvv: string;

}