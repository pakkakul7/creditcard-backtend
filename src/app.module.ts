import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CreditcardModule } from './creditcard/creditcard.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Creditcard} from './creditcard/creditcard';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '0000',
    database: 'card',
    entities: [Creditcard],
    synchronize: true,
  }), CreditcardModule ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }